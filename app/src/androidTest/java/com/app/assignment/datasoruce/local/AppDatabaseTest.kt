package com.app.assignment.datasoruce.local

import android.content.Context
import android.util.Log
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.app.assignment.model.User
import com.google.common.truth.Truth.assertThat
import junit.framework.TestCase
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Create by Deepika Dhammi on 01/02/22
 */

@RunWith(AndroidJUnit4::class)
class AppDatabaseTest : TestCase() {

    private lateinit var db: AppDatabase
    private lateinit var dao: UserDao


    @Before
    public override fun setUp() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(context,AppDatabase::class.java).build()
        dao=db.userDao()

    }

    @After
    fun closeDb(){
        db.close()
    }


    /**
     * pass case by putting values
     */
    @Test
    fun writeAndReadUser(){
        var user=User(1,"male","Garud Verma","verma_garud@satterfield.com","active")
        dao.insert(users = listOf(user))

        var getUsers=dao.getUsers()
        assertThat(getUsers?.contains(user)).isTrue()

    }

    /**
     * failed case
     */

    @Test
    fun writeAndReadUserFailed(){
        var user=User(0,"female","Deepika","dd@gmail.com","inactive")
        dao.insert(users = listOf(user))
        var getUsers=dao.getUsers()
        assertThat(getUsers?.contains(user)).isFalse()
    }

}