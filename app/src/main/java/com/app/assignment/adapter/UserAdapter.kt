package com.app.assignment.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.assignment.model.User

import androidx.databinding.DataBindingUtil
import com.app.assignment.R
import com.app.assignment.databinding.ItemUserBinding


/**
 * Create by Deepika Dhammi on 01/02/22
 */

class UserAdapter(private val list: ArrayList<User?>) :
    RecyclerView.Adapter<UserAdapter.UserViewHolder>() {

    class UserViewHolder(private val binding: ItemUserBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(user: User) {
            binding.user=user
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        val binding: ItemUserBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_user, parent, false)

        return UserViewHolder(binding)
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        list[position]?.let {
            holder.bind(it)
        }
    }

    fun updateData(newList: List<User?>) {
        val oldSize=list.size
        list.clear()
        notifyItemRangeRemoved(0,oldSize)
        list.addAll(newList)
        notifyItemRangeInserted(0,newList.size)
    }
}