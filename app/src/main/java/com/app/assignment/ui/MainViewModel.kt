package com.app.assignment.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.app.assignment.datasoruce.UserRemoteDataSource
import com.app.assignment.datasoruce.UserRepository
import com.app.assignment.model.ResponseUsers
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collect
import com.app.assignment.model.Result
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject


/**
 * Create by Deepika Dhammi on 01/02/22
 */

@HiltViewModel
class MainViewModel @Inject constructor(private val userRepository: UserRepository,userRemoteDataSource: UserRemoteDataSource) :
        ViewModel() {

    private val _userList = MutableLiveData<Result<ResponseUsers>>()
    val userList = _userList

    init {
        fetchUsers()
    }

    private fun fetchUsers() {
        viewModelScope.launch {
            userRepository.fetchUsers().collect {
                userList.value=it
            }
        }
    }
}