package com.app.assignment.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.assignment.R
import com.app.assignment.adapter.UserAdapter
import com.app.assignment.databinding.ActivityMainBinding
import kotlinx.android.synthetic.main.activity_main.*
import com.app.assignment.model.Result
import dagger.hilt.android.AndroidEntryPoint


/**
 * Create by Deepika Dhammi on 01/02/22
 */

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var mBinding: ActivityMainBinding
    private val viewModel by viewModels<MainViewModel>()
    private lateinit var userAdapter: UserAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding=DataBindingUtil.setContentView(this, R.layout.activity_main)

        initUI()
        getData()
    }

    private fun getData() {
        viewModel.userList.observe(this, Observer { result ->
            when (result.status) {
                Result.Status.SUCCESS -> {
                    result.data?.data?.let { list ->
                        userAdapter.updateData(list)
                    }
                    mBinding.loading.visibility = View.GONE
                }

                Result.Status.ERROR -> {
                    result.message?.let {
                        showError(it)
                    }
                    mBinding.loading.visibility = View.GONE
                }

                Result.Status.LOADING -> {
                    mBinding.loading.visibility = View.VISIBLE
                }
            }

        })
    }

    private fun initUI() {
        userAdapter= UserAdapter(ArrayList())
        mBinding.rvUsers.apply {
            layoutManager=LinearLayoutManager(
                this@MainActivity,
                LinearLayoutManager.VERTICAL, false)
            adapter=userAdapter
        }
    }

    private fun showError(msg: String) {
        Toast.makeText(this@MainActivity, msg, Toast.LENGTH_SHORT).show()
    }
}