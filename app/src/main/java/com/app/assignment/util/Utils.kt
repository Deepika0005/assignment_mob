package com.app.assignment.util

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import com.app.assignment.MyApp
import java.lang.Exception

/**
 * Create by Deepika Dhammi on 01/02/22
 */
class Utils {

    companion object {
        fun isNetworkConnected(): Boolean {
            val net: Boolean = try {
                val ConMgr =
                    MyApp.getContext().getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                (ConMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI)!!.state == NetworkInfo.State.CONNECTED
                        || ConMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)!!.state == NetworkInfo.State.CONNECTED)
            } catch (e: Exception) {
           e.printStackTrace()
                return false
            }
            return net
        }
    }
}