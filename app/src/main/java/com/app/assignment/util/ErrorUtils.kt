package com.app.assignment.util

import com.app.assignment.model.Error
import retrofit2.Response
import retrofit2.Retrofit
import java.io.IOException

/**
 * Create by Deepika Dhammi on 01/02/22
 */

/**
 * parses error response body
 */
object ErrorUtils {

    fun parseError(response: Response<*>, retrofit: Retrofit): Error? {
        val converter = retrofit.responseBodyConverter<Error>(Error::class.java, arrayOfNulls(0))
        return try {
            converter.convert(response.errorBody()!!)
        } catch (e: IOException) {
            Error()
        }
    }
}