package com.app.assignment

import android.app.Application
import android.content.Context
import dagger.hilt.android.HiltAndroidApp


/**
 * Create by Deepika Dhammi on 01/02/22
 */
@HiltAndroidApp
class MyApp: Application() {
    companion object {
        private lateinit var mContext: Context

        fun getContext() = mContext
    }

    override fun onCreate() {
        super.onCreate()
        mContext=applicationContext
    }
}