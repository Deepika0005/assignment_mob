package com.app.assignment.model

import com.google.gson.annotations.SerializedName

data class ResponseUsers(

	@field:SerializedName("data")
	val data: List<User?>? = null,

	@field:SerializedName("meta")
	val meta: Meta? = null
) {
	data class Meta(
		@field:SerializedName("pagination")
		val pagination: Pagination? = null
	) {
		data class Pagination(

			@field:SerializedName("total")
			val total: Int? = null,

			@field:SerializedName("pages")
			val pages: Int? = null,

			@field:SerializedName("limit")
			val limit: Int? = null,

			@field:SerializedName("links")
			val links: Links? = null,

			@field:SerializedName("page")
			val page: Int? = null
		) {
			data class Links(

				@field:SerializedName("next")
				val next: String? = null,

				@field:SerializedName("current")
				val current: String? = null,

				@field:SerializedName("previous")
				val previous: Any? = null
			)
		}
	}
}