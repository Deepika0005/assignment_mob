package com.app.assignment.network.service

import com.app.assignment.network.ApiConstants
import com.app.assignment.model.ResponseUsers
import retrofit2.Response
import retrofit2.http.GET


/**
 * Create by Deepika Dhammi on 01/02/22
 */
interface DataService {
    @GET(ApiConstants.USERS)
    suspend fun getUsers() : Response<ResponseUsers>
}