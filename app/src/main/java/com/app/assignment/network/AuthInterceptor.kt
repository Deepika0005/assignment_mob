package com.app.assignment.network

import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response


/**
 * Create by Deepika Dhammi on 01/02/22
 */

/**
 * Intercepts to clear auth on server
 */
class AuthInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val original = chain.request()
        val httpUrl = original.url.newBuilder()
            .build()

        val requestBuilder: Request.Builder = original.newBuilder()
            .url(httpUrl)

        return chain.proceed(requestBuilder.build())
    }
}