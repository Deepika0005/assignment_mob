package com.app.assignment.datasoruce.local

import androidx.room.*
import com.app.assignment.model.User

@Dao
interface UserDao {

    @Query("SELECT * FROM user")
    fun getUsers(): List<User>?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(users: List<User?>)

    @Delete
    fun delete(users: List<User?>)
}