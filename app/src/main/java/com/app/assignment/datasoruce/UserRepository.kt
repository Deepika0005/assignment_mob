package com.app.assignment.datasoruce

import com.app.assignment.model.ResponseUsers
import com.app.assignment.datasoruce.local.UserDao
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import com.app.assignment.model.Result
import com.app.assignment.util.Utils
import javax.inject.Inject


/**
 * Create by Deepika Dhammi on 01/02/22
 */
/**
 * Repository which fetches data from Remote or Local data sources
 */
class UserRepository @Inject constructor(
    private val userRemoteDataSource: UserRemoteDataSource,
    private val userDao: UserDao
) {

    suspend fun fetchUsers(): Flow<Result<ResponseUsers>?> {
        return flow {
            if (!Utils.isNetworkConnected())
                emit(fetchUsersCached())
            emit(Result.loading())
            val result = userRemoteDataSource.fetchUsers()
            //Cache to database if response is successful
            if (result.status == Result.Status.SUCCESS) {
                result.data?.data?.let {
                    userDao.delete(it)
                    userDao.insert(it)
                }
            }
            emit(result)
        }.flowOn(Dispatchers.IO)
    }

    private fun fetchUsersCached(): Result<ResponseUsers>? =
        userDao.getUsers()?.let {
            Result.success(ResponseUsers(it))
        }
}