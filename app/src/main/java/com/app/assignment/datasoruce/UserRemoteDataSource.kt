package com.app.assignment.datasoruce

import com.app.assignment.model.ResponseUsers
import com.app.assignment.network.service.DataService
import retrofit2.Response
import retrofit2.Retrofit
import javax.inject.Inject
import com.app.assignment.model.Result
import com.app.assignment.util.ErrorUtils



/**
 * Create by Deepika Dhammi on 01/02/22
 */

/**
 * fetches data from remote source
 */


class UserRemoteDataSource @Inject constructor(private val retrofit: Retrofit) {

    suspend fun fetchUsers(): Result<ResponseUsers> {
        val userService = retrofit.create(DataService::class.java);
        return getResponse(
                request = { userService.getUsers() },
                defaultErrorMessage = "Error fetching users list")
    }

    private suspend fun <T> getResponse(request: suspend () -> Response<T>, defaultErrorMessage: String): Result<T> {
        return try {
            val result = request.invoke()
            if (result.isSuccessful) {
                return Result.success(result.body())
            } else {
                val errorResponse = ErrorUtils.parseError(result, retrofit)
                Result.error(errorResponse?.status_message ?: defaultErrorMessage, errorResponse)
            }
        } catch (e: Throwable) {
            Result.error("Unknown Error", null)
        }
    }
}