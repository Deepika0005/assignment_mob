package com.app.assignment.datasoruce.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.app.assignment.model.User

@Database(entities = [User::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun userDao(): UserDao
}